# Getting started with Grunt

## Grunt Concat example

Create base `package.json` file
	
	{
	  "dependencies": {
	  
	  }
	}
	
Run `npm install --save grunt-contrib-concat` to update `package.json` file with grunt task

Create Gruntfile: `touch Gruntfile.js`

	module.exports = function (grunt) {
	    grunt.initConfig({
	        
	    });
	
	    grunt.loadNpmTasks('grunt-contrib-concat');
	};

Run to view 'Available Tasks' `grunt --help`  (no tasks found)
	
Add Tasks by updating Gruntfile.js

	module.exports = function (grunt) {
	    grunt.initConfig({
	        concat: {
	            dist: {
	                src: 'lib/**/*.js',
	                dest: 'dist/app.js'
	            }
	        }
	    });
	
	    grunt.loadNpmTasks('grunt-contrib-concat');
	};

Run `grunt --help` again to view 'Available Tasks
	`concat  Concatenate files. *`
	
Create `lib` js files

Run `grunt concat` to concat `lib/*.js` files into `dist`

Result should be a 'concat' of all `lib/*.js` files into `dist` directory